/*
 * Licentia by INRIA
 * (c) 2014 Cristian Cardellino for INRIA
 *
 * Licensed under Apache License, Version 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for more information.
 */

function sortConditions(a, b) {
  return a.label.localeCompare(b.label);
}

function sortLicenses(a, b) {
  return a.title.localeCompare(b.title);
}

$("#top-button").click(function(){
  $("html, body").animate({ scrollTop: 0 }, 500);
});

$(window).load(function(){
  if(navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)) {
    $('meta[name=viewport]').attr('content','width=1280');
  } else {
    $('meta[name=viewport]').attr('content','width=device-width');
  }
});
