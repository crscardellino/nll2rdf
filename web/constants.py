# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals


CLASS_MAP = {
    "PER": "Permission",
    "PRO": "Prohibition",
    "REQ": "Duty"
}


RULES_MAP = {
    "ATTACHPOLICY": "attachPolicy",
    "ATTACHSOURCE": "attachSource",
    "ATTRIBUTE": "attribute",
    "COMMERCIALIZE": "commercialize",
    "DERIVE": "derive",
    "DISTRIBUTE": "distribute",
    "READ": "read",
    "REPRODUCE": "reproduce",
    "SELL": "sell",
    "SHAREALIKE": "shareAlike"
}

RDF_PREFIX = \
    "@prefix :      <http://example/licenses> .\n" + \
    "@prefix l4lod: <http://ns.inria.fr/l4lod/> .\n" + \
    "@prefix odrl:  <http://www.w3.org/ns/odrl/2/> .\n" + \
    "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n\n"
