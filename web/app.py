# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import requests

from flask import Flask, render_template, request, Response

from .utils import limit_content_length, xml_to_rdf


app = Flask(__name__, static_url_path='/nll2rdf/static')
app.config.from_envvar('NLL2RDF_WEB_SETTINGS')


@app.route('/nll2rdf', methods=['GET', 'POST'])
@limit_content_length(256 * 1024)
def nll2rdf():
    if request.method == 'POST':
        data = {
            'name': request.form['name'],
            'url': request.form['url'],
            'terms': request.form['terms']
        }

        response = requests.post(
            app.config['API_BIND'],
            json={
                'license': data['terms'],
                'return_type': app.config['LICENSES_TAG_FORMAT']
            }
        )

        rdf_license = xml_to_rdf(response.text, data['name'], data['url'])
        return render_template('license_rdf.html', rdf_license=rdf_license)
    else:
        return render_template('license_upload.html')
