# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import tabulate

from flask import abort, request
from functools import wraps
from lxml import etree
from slugify import slugify

from .constants import CLASS_MAP, RULES_MAP, RDF_PREFIX

tabulate.PRESERVE_WHITESPACE = True


def limit_content_length(max_length):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            cl = request.content_length
            if cl is not None and cl > max_length:
                abort(413)
            return f(*args, **kwargs)
        return wrapper
    return decorator


def _compatible_rules(rdf_rules):
    # Simple checker until the model performance improves
    # Duty > Permission > Prohibition
    rdf_rules['Permission'] = [rule for rule in rdf_rules['Permission']
                               if rule not in rdf_rules['Duty']]
    rdf_rules['Prohibition'] = [rule for rule in rdf_rules['Prohibition']
                                if rule not in rdf_rules['Permission']
                                and rule not in rdf_rules['Duty']]

    return rdf_rules


def xml_to_rdf(raw_text, name, url):
    xml_license = etree.fromstring(raw_text)

    labels = [word.attrib['label'].split('|')
              for word in xml_license.xpath('.//word')
              if word.attrib['label'] != 'O']
    labels = set(tuple(lbl.split('-')) for lbls in labels for lbl in lbls)

    rdf_rules = {
        "Duty": [],
        "Permission": [],
        "Prohibition": []
    }

    for klass, rule in labels:
        rdf_rules[CLASS_MAP[klass]].append(RULES_MAP[rule])

    rdf = [[":%s" % slugify(name), "a", "odrl:Set ;"]]
    rdf += [["", "rdfs:label", "\"%s\" ;" % name]]
    rdf += [["", "l4lod:licensingTerms", "<%s> ;" % url]]

    for klass, rules in sorted(_compatible_rules(rdf_rules).items()):
        if len(rules):
            rdf += [["", "odrl:%s" % klass.lower(), "[ a odrl:%s ;" % klass]]
            rdf += [["", "",
                     "  odrl:action %s" %
                     (" , ".join("odrl:%s" % rule for rule in sorted(rules)))]]
            rdf += [["", "", "] ;"]]

    # Change last ";" for a "."
    rdf = tabulate.tabulate(rdf, tablefmt="plain").strip()[:-1] + '.'

    return RDF_PREFIX + rdf
