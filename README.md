NLL2RDF
=======

Web API
-------

- Serve the app via gunicorn:

		gunicorn api.app:app --bind=127.0.0.1:8000 --reload

- Serve the app locally

		python -m api.app


Setting up
----------

## Instructions

1. Create the directories:
	- `sudo mkdir -p /opt/nll2rdf/data/{models,user_files}`
2. Build and copy the NLL2RDF directory:
    - `cd ./nll2rdf/`
    - `bash update_nll2rdf.sh`
    - `sudo rsync -aP --exclude "*.pyc" --exclude "__pycache__" --delete ./api /opt/nll2rdf`
    - `sudo rsync -aP --delete ./data/models/ /opt/nll2rdf/data/models`
    - `sudo rsync -aP --exclude "*.pyc" --exclude "__pycache__" ./web /opt/nll2rdf`
3. Create and setup virtual environment:
	- `$ sudo dnf install gcc-c++ @development-tools`
	- `$ sudo dnf install python-devel python3-devel python3-pip`
	- `$ sudo pip3 install virtualenv # (can be sudo python3-pip instead of pip3)`
	- `$ sudo /usr/local/bin/virtualenv --python=/usr/bin/python3 /opt/nll2rdf/nll2rdf-env`
	- `$ sudo su`
	- `# source /opt/nll2rdf/nll2rdf-env/bin/activate`
	- `# pip install ipython`
	- `# pip install -r path/to/user/home/nll2rdf/requirements.txt`
	- `# pip update msgpack # only if fails to load spacy nlp`
	- `# python -m spacy download en`
4. Create the Daemon users and assign the corresponding ownerships:
	- `sudo /usr/sbin/groupadd -r nll2rdf`
	- `sudo /usr/sbin/useradd -M -r -d / -s /sbin/nologin -c "NLL2RDF Daemon" -g nll2rdf nll2rdf`
	- `sudo chown -R nll2rdf:nll2rdf /opt/nll2rdf/`
5. Copy the configuration and service files to the corresponding directories and enable and start/reload:
	- `sudo cp $HOME/nll2rdf/server_configs/systemd/*.service /etc/systemd/system`
	- `sudo systemctl daemon-reload`
	- `sudo systemctl enable nll2rdf-api.service`
	- `sudo systemctl start nll2rdf-api.service`
	- `sudo systemctl enable nll2rdf-web.service`
	- `sudo systemctl start nll2rdf-web.service`
	- `sudo systemctl restart nginx.service`
