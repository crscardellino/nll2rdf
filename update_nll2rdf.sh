#!/usr/bin/env bash

git pull

mkdir -p api/lib/
mkdir -p data/models/
mkdir -p data/user_files/

rsync -aP --exclude "*.pyc" --exclude "__pycache__" --delete nll2rdf api/lib/
