#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import argparse
import sys

from nll2rdf.pipeline import Pipeline


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='NLL2RDF Train script (C) 2014-2018 '
                    'Cristian Cardellino for INRIA.')
    parser.add_argument('input',
                        type=str,
                        help='Path to the xml file with the licences.')
    parser.add_argument('output',
                        type=str,
                        help='Path to save the trained model.')
    parser.add_argument('--folds',
                        type=int,
                        default=3,
                        help='Number of folds for the classifier.')
    parser.add_argument('--test-split',
                        type=float,
                        default=0,
                        help='Percentage of corpus reserved for test.')
    parser.add_argument('--min-count',
                        type=int,
                        default=3,
                        help='Minimum number of occurrences for a label to '
                             'be considered.')
    parser.add_argument('--no-entity-sampling',
                        type=float,
                        default=0.1,
                        help='Ratio for sampling sentences without entities.')

    args = parser.parse_args()

    print('Loading the pipeline', file=sys.stderr)
    pipe = Pipeline(document_path=args.input,
                    folds=args.folds,
                    reader=('XMLDocumentReader',
                            {'label_minimum_count': args.min_count,
                             'no_entity_sampling': args.no_entity_sampling}),
                    test_split=args.test_split)

    print('Fitting the pipeline', file=sys.stderr)
    pipe.fit()

    print('Saving the pipeline', file=sys.stderr)
    pipe.save(args.output)

    if args.test_split > 0:
        print('TEST RESULTS\n============', file=sys.stderr)
        for metric in ('accuracy', 'precision', 'recall', 'f1'):
            print('Evaluation Metric: %s' % metric, file=sys.stderr)
            for label, evaluation in sorted(pipe.evaluate(metric).items()):
                print('%s: %.2f' % (label, evaluation), file=sys.stderr)
            print(file=sys.stderr)
