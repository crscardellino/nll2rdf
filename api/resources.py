# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import falcon
import json
import os
import uuid

from nll2rdf.pipeline import Pipeline

from . import settings


def max_body(limit):
    def hook(req, resp, resource, params):
        length = req.content_length
        if length is not None and length > limit:
            msg = ('The size of the request is too large. The body must not '
                   'exceed ' + str(limit) + ' bytes in length.')

            raise falcon.HTTPRequestEntityTooLarge(
                'Request body is too large', msg)

    return hook


class LicenseClassifierResource(object):
    _return_type = {
        'conll': falcon.MEDIA_TEXT,
        'json': falcon.MEDIA_JSON,
        'xml': falcon.MEDIA_XML
    }

    def __init__(self):
        self._pipe = Pipeline.load(settings.NER_MODEL_FILE)

    @falcon.before(max_body(256 * 1024))
    def on_post(self, req, resp):
        """
        :param falcon.Request req: Request object to read.
        :param falcon.Response resp: Response object to write.
        """

        try:
            data = json.loads(req.bounded_stream.read())
        except json.JSONDecodeError:
            msg = 'Invalid JSON request.'
            raise falcon.HTTPBadRequest('Bad Request', msg)

        if data.keys() != {'license', 'return_type'}:
            msg = 'Invalid JSON format.'
            raise falcon.HTTPBadRequest('Bad Request', msg)

        resp.status = falcon.HTTP_OK
        resp.content_type = self._return_type[data['return_type']]
        categorized_text = self._pipe.categorize(data['license'],
                                                 data['return_type'])
        save_path = os.path.join(
            settings.USER_FILES,
            '%s.%s' % (uuid.uuid4(), data['return_type'])
        )

        with open(os.path.join(settings.USER_FILES, save_path), 'w') as fh:
            fh.write(categorized_text)

        resp.body = categorized_text
