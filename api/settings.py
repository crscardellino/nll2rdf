# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import os


PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

# Data directory
DATA_ROOT = os.path.join(PROJECT_ROOT, 'data')

USER_FILES = os.path.join(PROJECT_ROOT, 'data', 'user_files')

# Model files
NER_MODEL_FILE = os.path.join(DATA_ROOT, 'models', 'crf.pkl')
