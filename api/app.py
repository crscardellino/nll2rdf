#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, unicode_literals

import falcon
import os
import sys

# TODO: Convert this into a right library by defining a setup.py
sys.path.append('%s/lib/' % os.path.dirname(os.path.abspath(__file__)))

from .resources import LicenseClassifierResource

# Create falcon app
app = falcon.API()

# Resources
clf = LicenseClassifierResource()

# Routes
app.add_route('/', clf)

# Useful for debugging problems in API, it works with pdb
if __name__ == '__main__':
    from wsgiref import simple_server
    httpd = simple_server.make_server('127.0.0.1', 8000, app)
    print('Listening locally on port 8000', file=sys.stderr)
    httpd.serve_forever()
