# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals


class HandcraftedFeatureExtractor(object):
    def __init__(self, current_word=True, previous_word=True, next_word=True,
                 character_ngrams='all', current_pos=True, surrounding_pos=True,
                 current_shape=True, surrounding_shape=True, presence_left=10,
                 presence_right=10, current_lemma=True, dependency=True,
                 use_coarse_grained_pos=True):
        """
        Extractor of handcrafted features, based upon the default features for
        the NER system of Stanford, described in:
        "Incorporating Non-local Information into Information Extraction
            Systems by Gibbs Sampling."

        :param bool current_word: Current word.
        :param bool previous_word: Previous word.
        :param bool next_word: Next word.
        :param bool character_ngrams: Consider the prefixes and suffixes formed
            by n-grams of characters in the word. Can be the word 'all' to use
            the whole affixes available for each word.
        :param bool current_pos: Part-of-speech of the current word.
        :param bool surrounding_pos: Part-of-speech tags of the surrounding
            words.
        :param bool current_shape: Shape of the current word.
        :param bool surrounding_shape: Shape of the surrounding words.
        :param int presence_left: Maximum number of words present in the left
            window.
        :param int presence_right: Maximum number of words present in the right
            window.
        :param bool current_lemma: Lemma of the current word.
        :param bool dependency: Dependency triple of the current word.
        :param bool use_coarse_grained_pos: Whether to use coarse or fine
            grained part-of-speech tags.
        """
        assert character_ngrams == 'all' or isinstance(character_ngrams, int)

        self._current_word = current_word
        self._current_pos = current_pos
        self._current_shape = current_shape
        self._current_lemma = current_lemma
        self._previous_word = previous_word
        self._next_word = next_word
        self._surrounding_pos = surrounding_pos
        self._surrounding_shape = surrounding_shape
        self._character_ngrams = character_ngrams
        self._presence_left = presence_left
        self._presence_right = presence_right
        self._dependency = dependency
        self._use_coarse_grained_pos = use_coarse_grained_pos

    def featurize_word(self, sentence, widx):
        """
        Featurize the widx-th word in the sentence.

        :param list[nll2rdf.utils.Word] sentence: List of Words
        :param int widx: Index to the word to featurize
        :return: The featurized word according to the
        """
        word = sentence[widx]
        features = {}

        if self._current_word:
            features['word'] = word.token

        if self._current_pos and self._use_coarse_grained_pos:
            features['pos'] = word.pos
        elif self._current_pos:
            features['pos'] = word.tag

        if self._current_shape:
            features['shape'] = word.shape

        if self._previous_word and widx > 0:
            idx = widx - 1
            features['-1:word'] = sentence[idx].token

            if self._surrounding_pos and self._use_coarse_grained_pos:
                features['-1:pos'] = sentence[idx].pos
            elif self._surrounding_pos:
                features['-1:pos'] = sentence[idx].tag

            if self._surrounding_shape:
                features['-1:shape'] = sentence[idx].shape

        if self._next_word and widx < len(sentence)-1:
            idx = widx + 1
            features['+1:word'] = sentence[idx].token

            if self._surrounding_pos and self._use_coarse_grained_pos:
                features['+1:pos'] = sentence[idx].pos
            elif self._surrounding_pos:
                features['+1:pos'] = sentence[idx].tag

            if self._surrounding_shape:
                features['+1:shape'] = sentence[idx].shape

        if self._character_ngrams:
            prefixes, suffixes = word.get_affixes(self._character_ngrams)

            for i, prefix in enumerate(prefixes):
                features['prefix:%02d' % i] = prefix

            for i, suffix in enumerate(suffixes):
                features['suffix:%02d' % i] = suffix

        for i in range(1, self._presence_left+1):
            idx = widx-i
            if idx > 0:
                features['left:%s' % sentence[idx].token] = True

        for i in range(1, self._presence_right+1):
            idx = widx+i
            if idx < len(sentence):
                features['right:%s' % sentence[idx].token] = True

        return features

    def featurize_sentence(self, sentence):
        """
        Featurize a sentence.

        :param list[nll2rdf.utils.Word] sentence: Sentence to featurize.
        :return: Featurized sentence word by word.
        """
        return [self.featurize_word(sentence, widx)
                for widx in range(len(sentence))]
