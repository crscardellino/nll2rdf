# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, print_function, unicode_literals

import argparse
import fnmatch
import os
import re
import sys

from collections import defaultdict, namedtuple
from lxml import etree
from tqdm import tqdm

_CLASSES = {
    "DUTY": "REQ",
    "PERMISSION": "PER",
    "PERMITS": "PER",
    "PROHIBITION": "PRO",
    "PROHIBITS": "PRO",
    "REQUIRES": "REQ"
}

_RULES = {
    "ATTACHPOLICY": "ATTACHPOLICY",
    "ATTACHSOURCE": "ATTACHSOURCE",
    "ATTRIBUTE": "ATTRIBUTE",
    "ATTRIBUTION": "ATTRIBUTE",
    "COMMERCIALIZE": "COMMERCIALIZE",
    "COMMERCIALUSE": "COMMERCIALIZE",
    "COPY": "REPRODUCE",
    "DERIVATIVEWORKS": "DERIVE",
    "DERIVE": "DERIVE",
    "DISTRIBUTE": "DISTRIBUTE",
    "DISTRIBUTION": "DISTRIBUTE",
    "MODIFY": "DERIVE",
    "NOTICE": "ATTACHPOLICY",
    "READ": "READ",
    "REPRODUCE": "REPRODUCE",
    "REPRODUCTION": "REPRODUCE",
    "SELL": "SELL",
    "SHARE": "DISTRIBUTE",
    "SHAREALIKE": "SHAREALIKE",
    "SHARING": "DISTRIBUTE"
}

_Word = namedtuple('_Word', ['token', 'klass', 'rule'])


def get_target_label(class_name, rule_name):
    return '%s-%s' % (_CLASSES[class_name], _RULES[rule_name])


def find_files(path, file_pattern='*'):
    for root, _, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, file_pattern):
            yield os.path.join(root, filename)


class _CONLLLicensesDocumentReader(object):
    """
    CONLL Format document reader. For now only useful to transform to XML and work more easily.
    """

    _default_columns = {
        'idx': 0,
        'token': 1,
        'lemma': 2,
        'pos': 3,
        'head': 6,
        'dep': 7,
        'class': 10,
        'rule': 11
    }

    def __init__(self, document_path, columns=None, meta=r'^#'):
        self._document_path = document_path
        self._columns = self._default_columns if columns is None else columns
        self._meta = meta
        self.document_label = None

    def __iter__(self):
        with open(self._document_path, 'r') as fh:
            words = []

            for idx, line in enumerate(fh, start=1):
                if re.match(self._meta, line.strip()):
                    continue  # Ignore for now

                word = line.strip().split()

                if len(word) == 0:
                    yield words[:]
                    words = []
                    continue
                elif len(word) == 11:
                    token = word[self._columns['token']]
                    klass = word[self._columns['class']]
                    rule = None
                elif len(word) == 12:
                    assert self.document_label is None, \
                        "There is more than one document label for document %s on line %d" % \
                        (self._document_path, idx)
                    try:
                        self.document_label = get_target_label(
                            word[self._columns['class']][2:],
                            word[self._columns['rule']]
                        )
                    except KeyError:
                        raise KeyError('There is an inconsistent word for document %s on line %d' %
                                       (self._document_path, idx))
                    token = word[self._columns['token']]
                    klass = word[self._columns['class']]
                    rule = word[self._columns['rule']]
                else:
                    raise Exception('There is an inconsistent word for document %s on line %d' %
                                    (self._document_path, idx))

                assert self.document_label is not None or klass == 'O', \
                    "The document label hasn't been set yet for document %s on line %d" % \
                    (self._document_path, idx)

                if token in {'-LRB-', '-LSB-'}:
                    token = '('
                elif token in {'-RRB-', '-RSB-'}:
                    token = ')'

                words.append(_Word(token, klass, rule))

            if len(words) != 0:
                yield words[:]


if __name__ == '__main__':
    parser = argparse.ArgumentParser('NLL2RDF CONLL to XML converter')
    parser.add_argument('path',
                        type=str,
                        help='Path to the base directory where the licences are stored.')
    parser.add_argument('--extension',
                        type=str,
                        default='conll',
                        help='File extension of the licences. Default: conll.')
    parser.add_argument('--output',
                        type=str,
                        default=None,
                        help='File to output data. Default: STDOUT.')

    args = parser.parse_args()

    xml_root = etree.Element('licenses')
    xml_docs = etree.SubElement(xml_root, 'documents')
    document_labels = defaultdict(int)

    for didx, dpath in enumerate(tqdm(sorted(find_files(args.path, '*.%s' % args.extension))), start=1):
        document = _CONLLLicensesDocumentReader(dpath)
        document_name, _ = os.path.basename(dpath).split('.%s' % args.extension, 1)
        xml_doc = etree.SubElement(xml_docs, 'document',
                                   {'name': document_name,
                                    'idx': '%03d' % didx})

        for sidx, sentence in enumerate(document, start=1):
            xml_sent = etree.SubElement(xml_doc, 'sentence',
                                        {'idx': '%03d-%02d' % (didx, sidx)})

            for widx, word_ in enumerate(sentence, start=1):
                attribs = {
                    'idx': '%03d-%02d-%02d' % (didx, sidx, widx),
                    'token': word_.token,
                    'class': word_.klass
                }

                if word_.rule is not None:
                    attribs['rule'] = word_.rule

                xml_word = etree.SubElement(xml_sent, 'word', attribs)

        assert document.document_label is not None, "Document %s doesn't have a label" % dpath

        xml_doc.attrib['label'] = document.document_label
        document_labels[document.document_label] += 1

    xml_labels = etree.Element('labels')
    for lidx, label in enumerate(tqdm(sorted(document_labels)), start=1):
        xml_label = etree.SubElement(xml_labels, 'label',
                                     {'tag': label,
                                      'count': '%d' % document_labels[label],
                                      'idx': '%02d' % lidx})

    xml_root.insert(0, xml_labels)

    if args.output is not None:
        output_file = open(args.output, 'w')
    else:
        output_file = sys.stdout

    print(etree.tostring(xml_root, encoding='unicode', pretty_print=True), file=output_file)

    if args.output is not None:
        output_file.close()
