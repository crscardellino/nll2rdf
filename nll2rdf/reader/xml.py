# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import numpy as np

from lxml import etree
from nll2rdf.utils import Word


class XMLDocumentReader(object):
    def __init__(self, document_path, label_minimum_count=3,
                 no_entity_sampling=0.1):
        """
        Reader for XML files.

        :param str document_path: Path to the input document.
        :param int label_minimum_count: Minimum number of occurrences for a
            label to be considered.
        :param float no_entity_sampling: Ratio of sentences without entities
            to subsample.
        """
        self._root = etree.parse(document_path).getroot()
        self._label_minimum_count = label_minimum_count
        self._no_entity_sampling = no_entity_sampling

    def get_sentences_for_label(self, label):
        """
        Get the sentences for a single label

        :param str label: Label to return the sentences from.
        :return list[Word]: For each sentence yields a list of Word.
        """
        for document in self._root.find('documents')\
                .xpath('./document[@label="%s"]' % label):
            for sentence in document:
                if np.random.random() > self._no_entity_sampling and\
                        all([w.attrib['class'] == 'O' for w in sentence]):
                    # When all the words are non-entities omit if the filter
                    # flag is set
                    continue
                else:
                    # Return each sentence as a list of tuples where each tuple
                    # represents a word with the class (correctly mapped) only
                    # in I/O format (simpler because of the low number of
                    # sentences)

                    yield [Word(w.attrib['token'], 'I-%s' % label
                                if w.attrib['class'] != 'O' else 'O')
                           for w in sentence]

    def get_sentences(self):
        """
        Get the sentences for each of the labels

        :return (str, list[list[Word]]): For each label yields the tuple with
            the label name and the instances of the sentences for that label.
        """
        for label in self._root.find('labels'):
            if int(label.attrib['count']) >= self._label_minimum_count:
                yield label.attrib['tag'], self.get_sentences_for_label(
                    label.attrib['tag'])
