# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

from .xml import XMLDocumentReader


__all__ = ['XMLDocumentReader']
