# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals


class Word(object):
    def __init__(self, token, label='-', lemma='-', pos='-', tag='-', shape='-',
                 head='-', dep='-', is_stop=False):
        """
        Class to represent a word in the NER corpus.

        :param str token: Raw word.
        :param str label: Label of the word (if applicable).
        :param str lemma: Lemma of the word.
        :param str pos: Coarse grained part-of-speech tag of the word.
        :param str tag: Fine grained part-of-speech tag of the word.
        :param str shape: Shape of the word.
        :param str head: Head of the word in the dependency triple.
        :param str dep: Dependency relation.
        :param bool is_stop: Whether the word is a stopword for the language.
        """
        self.token = token
        self.label = label
        self.lemma = lemma
        self.pos = pos
        self.tag = tag
        self.shape = shape
        self.head = head
        self.dep = dep
        self.is_stop = is_stop

        self._tuple = ('token', 'label', 'lemma', 'pos', 'tag', 'shape', 'head',
                       'dep', 'is_stop')

    def __getitem__(self, item):
        """
        Returns the item in the word seen as a tuple of elements.
        :param int item: Index of the item.
        :return: Returns the item in the tuple.
        """
        return getattr(self, self._tuple[item])

    def __repr__(self):
            return '\t'.join(str(getattr(self, item)) for item in self._tuple)

    def get_affixes(self, max_characters='all'):
        """
        Get the affixes of the word as n-grams of characters.

        :param int|str max_characters: The maximum number of characters to
            consider (if 'all' then take all else a positive integer)
        :return: The prefixes and suffixes of the word taking characters from
            1 to max_character.

        Precondition
        ------------
        max_characters must be an integer or the word 'all'
        """

        assert max_characters == 'all' or isinstance(max_characters, int)

        prefixes = []
        suffixes = []

        max_characters = len(self.token) \
            if max_characters == 'all' else max_characters

        for i in range(1, min(len(self.token), max_characters+1)):
            prefixes.append(self.token[:i])
            suffixes.append(self.token[-i:])

        return prefixes, suffixes
