# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import json

from lxml import etree
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split

from .feature_extraction import HandcraftedFeatureExtractor
from .model import CRFNEClassifier
from .parser import SpacyParser
from .reader import XMLDocumentReader
from .utils import Word


class Pipeline(object):
    _featurizer_map = {
        'HandcraftedFeatureExtractor': HandcraftedFeatureExtractor
    }

    _clf_map = {
        'CRFNEClassifier': CRFNEClassifier
    }

    _parser_map = {
        'SpacyParser': SpacyParser
    }

    _reader_map = {
        'XMLDocumentReader': XMLDocumentReader
    }

    def __init__(self, document_path=None, folds=3,
                 reader=('XMLDocumentReader', None),
                 parser=('SpacyParser', None),
                 featurizer=('HandcraftedFeatureExtractor', None),
                 clf=('CRFNEClassifier', None), models=None,
                 test_split=0):
        """
        Pipeline to create and classify data in the nll2rdf pipeline.

        The pipeline is only trainable once. Specially in cases where it is
        loaded from disk it should be already trained and it can't be retrained
        (as is, one can access the models and modify them as needed).

        :param str document_path: Path to the document to read with the reader.
            Can be None for cases where the pipeline is only needed for
            classification but the models were already trained (e.g. when
            loading from disk).
        :param int folds: Number of folds to do classification by vote in the
            models.
        :param (str, dict[str, T]) reader: Document reader where the data is
             taken from.
        :param (str, dict[str, T]) parser: Sentences parser.
        :param (str, dict[str, T]) featurizer: Sentences featurizer.
        :param (str, dict[str, T]) clf: Classification algorithm.
        :param dict[str, T] models: Dictionary of model per label.
        :param float test_split: Ratio of data to left for test split. Must be
            a floating point number in the range [0, 1)
        """
        assert 0 <= test_split < 1

        self._pipeline_configuration = dict(
            parser=parser,
            featurizer=featurizer,
        )

        # Only if there's a document path
        reader_cls = self._reader_map[reader[0]] if document_path else None
        reader_config = reader[1] if reader[1] else {}
        self._reader = reader_cls(document_path, **reader_config) \
            if document_path else None

        parser_cls = self._parser_map[parser[0]]
        parser_config = parser[1] if parser[1] else {}
        self._parser = parser_cls(**parser_config)

        featurizer_cls = self._featurizer_map[featurizer[0]]
        featurizer_config = featurizer[1] if featurizer[1] else {}
        self._featurizer = featurizer_cls(**featurizer_config)

        # Only if there's a document path
        self._clf_cls = self._clf_map[clf[0]] if document_path else None
        self._clf_conf = clf[1] if clf[1] is not None else {}
        self._folds = folds

        self.models = models

        # Only if there's a document path
        self._test_split = test_split if document_path else None
        self._test_datasets = None

    def fit(self):
        """
        Fits all the labels for each model. The model should not been already
        trained.
        """
        assert self.models is None, "The models were already trained"

        self.models = {}
        self._test_datasets = {}

        for label, sentences in self._reader.get_sentences():
            parsed_sentences = [self._parser.parse_sentence(s)
                                for s in sentences]
            data = [self._featurizer.featurize_sentence(s)
                    for s in parsed_sentences]
            target = [[word.label for word in sentence]
                      for sentence in parsed_sentences]

            if self._test_split > 0:
                # For evaluation purposes
                data, test_data, target, test_target =\
                    train_test_split(data, target, test_size=self._test_split)
                self._test_datasets[label] = (test_data, test_target)

            self.models[label] = self._clf_cls(
                folds=self._folds, **self._clf_conf).fit(data, target)

        return self

    def _conll_categorize(self, text):
        """
        Evaluates and predicts each model on free text.

        :param str text: The text categorize.
        :return: The parsed text in conll format.
        """

        conll_sentences = []

        for sentence in self._parser.parse_text(text):
            fsentence = self._featurizer.featurize_sentence(sentence)

            sentence_predictions = [
                model.predict([fsentence])[0]
                for _, model in sorted(self.models.items())
            ]

            predictions = []
            for word_predictions in zip(*sentence_predictions):
                labels = sorted(set(word_predictions))

                if len(labels) == 1:  # Only one label, doesn't matter which
                    predictions.append(labels[0][2:]  # Remove the 'I-'
                                       if labels[0] != 'O' else 'O')
                elif len(labels) > 0:
                    if 'O' in labels:
                        labels.remove('O')  # With one I label discard 'O'
                    predictions.append('\t'.join(labels))

            words = []
            for wi, (word, label) in enumerate(zip(sentence, predictions),
                                               start=1):  # type: int, Word, str
                words.append('\t'.join([
                    '%02d' % wi,
                    word.token,
                    word.lemma,
                    word.pos,
                    word.tag,
                    '-',
                    word.head,
                    word.dep,
                    '-',
                    label
                ]))

            conll_sentences.append('\n'.join(words))

        return '\n\n'.join(conll_sentences)

    def _json_categorize(self, text):
        """
        Evaluates and predicts each model on free text.

        :param str text: The text categorize.
        :return: A json string with the sentences and words labeled
        """

        json_sentences = []

        for si, sentence in enumerate(self._parser.parse_text(text), start=1):
            fsentence = self._featurizer.featurize_sentence(sentence)

            sentence_predictions = [
                model.predict([fsentence])[0]
                for _, model in sorted(self.models.items())
            ]

            predictions = []
            for word_predictions in zip(*sentence_predictions):
                labels = sorted(set(word_predictions))

                if len(labels) == 1:  # Only one label, doesn't matter which
                    predictions.append(labels[0][2:]  # Remove the 'I-'
                                       if labels[0] != 'O' else 'O')
                else:
                    labels.remove('O')  # With one I label discard 'O'
                    predictions.append('|'.join(label[2:] for label in labels))

            words = []

            for wi, (word, label) in enumerate(zip(sentence, predictions),
                                               start=1):  # type: int, Word, str
                words.append({
                    'idx': '%03d-%02d' % (si, wi),
                    'token': word.token,
                    'lemma': word.lemma,
                    'pos': word.pos,
                    'tag': word.tag,
                    'shape': word.shape,
                    'head': word.head,
                    'dep': word.dep,
                    'stop': 'true' if word.is_stop else 'false',
                    'label': label
                })

            json_sentences.append({'idx': '%03d' % si, 'words': words})

        return json.dumps(json_sentences)

    def _xml_categorize(self, text):
        """
        Evaluates and predicts each model on free text.

        :param str text: The text categorize.
        :return: An xml tree with the sentences and words labeled
        """

        xml_root = etree.Element('license')

        for si, sentence in enumerate(self._parser.parse_text(text), start=1):
            fsentence = self._featurizer.featurize_sentence(sentence)

            sentence_predictions = [
                model.predict([fsentence])[0]
                for _, model in sorted(self.models.items())
            ]

            predictions = []
            for word_predictions in zip(*sentence_predictions):
                labels = sorted(set(word_predictions))

                if len(labels) == 1:  # Only one label, doesn't matter which
                    predictions.append(labels[0][2:]  # Remove the 'I-'
                                       if labels[0] != 'O' else 'O')
                else:
                    labels.remove('O')  # With one I label discard 'O'
                    predictions.append('|'.join(label[2:] for label in labels))

            xml_sentence = etree.SubElement(xml_root, 'sentence',
                                            {'idx': '%03d' % si})

            for wi, (word, label) in enumerate(zip(sentence, predictions),
                                               start=1):  # type: int, Word, str
                attribs = {
                    'idx': '%03d-%02d' % (si, wi),
                    'token': word.token,
                    'lemma': word.lemma,
                    'pos': word.pos,
                    'tag': word.tag,
                    'shape': word.shape,
                    'head': word.head,
                    'dep': word.dep,
                    'stop': 'true' if word.is_stop else 'false',
                    'label': label
                }

                xml_word = etree.SubElement(xml_sentence, 'word', attribs)

        return etree.tostring(xml_root, encoding='unicode')

    def categorize(self, text, return_type='json'):
        """
        Evaluates and predicts each model on free text.

        :param str text: The text categorize.
        :param str return_type: The return type format.
        :return: An xml tree or a json string with the sentences and words
        labeled.
        """
        assert return_type in ('conll', 'json', 'xml'),\
            'The return type is invalid.'

        if return_type == 'conll':
            return self._conll_categorize(text)
        elif return_type == 'json':
            return self._json_categorize(text)
        else:
            return self._xml_categorize(text)

    def evaluate(self, metric, average=None):
        """
        Evaluates all the models with the test datasets gathered in the fit
        step. It is only doable when there was some data left for splitting.

        :param str metric: A valid metric of accuracy, precision, recall and f1
        :param str average: The type of average (None, weighted, macro, micro)
        :return: A dictionary with the metric value for each of the models for
            each of the labels.
        """
        assert self._test_datasets, "There are no test datasets"

        results = {}

        for label in sorted(self._test_datasets):
            data, true = self._test_datasets[label]
            eval_ = self.models[label].evaluate(data, true, metric, average)

            if average is None and metric != 'accuracy':
                eval_ = eval_[0]

            results[label] = eval_

        return results

    @staticmethod
    def load(path):
        """
        Loads the pipeline from the given path.

        :param path: Path to the pickle file.
        :return: The loaded Pipeline.
        """
        pipeline_config, models = joblib.load(path)

        return Pipeline(models=models, **pipeline_config)

    def save(self, path):
        """
        Saves the pipeline in the given path. Only saves the dictionary of
        models and the info required to create a basic pipeline for a prediction
        only way.

        :param path:
        :return:
        """
        return joblib.dump(
            (self._pipeline_configuration, self.models),
            path
        )
