# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

import spacy

from nll2rdf.utils import Word
from spacy.tokens import Doc, Token


class SpacyParser(object):
    def __init__(self, language='en'):
        """
        Parser of sentences using Spacy's model.

        :param language: Spacy language to use.
        """
        self._nlp = spacy.load(language, disable=['ner'])

    def parse_sentence(self, sentence):
        """
        Parse a sentence with Spacy and returns the information.

        :param list[Word] sentence: A sentence is composed of a list of Word
        :return: returns the same sentence with the extra information given by
         the parser
        """
        document = Doc(self._nlp.vocab, words=[w.token for w in sentence])
        document = self._nlp.tagger(document)
        document = self._nlp.parser(document)
        labels = [w.label for w in sentence]

        sentence = []

        for tidx, token in enumerate(document):  # type: int, Token
            sentence.append(Word(
                token=token.text,
                label=labels[tidx],
                lemma=token.lemma_,
                pos=token.pos_,
                tag=token.tag_,
                shape=token.shape_,
                head=token.head.text,
                dep=token.dep_,
                is_stop=token.is_stop
            ))

        return sentence

    def parse_text(self, text):
        """
        Parse a free text with Spacy and returns the information for each
        sentence.

        :param string text: Text to parse and return information.
        :return: returns the parsed text as a list of sentences.
        """

        document = self._nlp(text)  # type: Doc

        for sentence in document.sents:  # type: list[Token]
            yield [
                Word(
                    token=token.text,
                    lemma=token.lemma_,
                    pos=token.pos_,
                    tag=token.tag_,
                    shape=token.shape_,
                    head=token.head.text,
                    dep=token.dep_,
                    is_stop=token.is_stop
                ) for token in sentence
            ]
