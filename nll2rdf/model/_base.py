# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

from abc import ABCMeta
from abc import abstractmethod


class NamedEntityClassifier(metaclass=ABCMeta):
    """
    Abstract base class for named entity classifiers using KFold counting (i.e.
    bagging)
    """
    _metrics = {}

    def __init__(self, folds=3, **model_config):
        """
        :param int folds: Number of folds to fit the model. It needs to be odd
            as this are needed for voting.
        :param dict model_config: Configuration to give to the model.
        """

        assert folds >= 1 and folds % 2 == 1, "The number of folds must be odd"

        self._folds = folds
        self._models = []
        self._model_config = model_config

    @abstractmethod
    def fit(self, data, target):
        """
        Fits the models

        Parameters
        ----------
        :param list[list[dict[str, T]]] data: The data, each element of the
            dictionary represents a word's features, a list of words represents
            a sentence.
        :param list[list[str]] target: the labels of each word for each
            sentence.
        """

    @abstractmethod
    def predict(self, data):
        """
        Returns the predictions of the model

        Parameters
        ----------
        :param list[list[dict[str, T]]] data: The data, each element of the
            dictionary represents a word's features, a list of words represents
            a sentence.
        """

    @abstractmethod
    def evaluate(self, data, true, metric='accuracy', average=None):
        """
        Evaluates the performance of the model

        Parameters
        ----------
        :param list[list[dict[str, T]]] data: The data, each element of the
            dictionary represents a word's features, a list of words represents
            a sentence.
        :param list[list[str]] true: the true labels of each word for each
             sentence.
        :param str metric: One of the possible metrics to use (accuracy,
            precision, recall, f1-score).
        :param str average: Type of average.
        """
