# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

from sklearn.model_selection import KFold
from sklearn_crfsuite import CRF, metrics
from ._base import NamedEntityClassifier


class CRFNEClassifier(NamedEntityClassifier):
    """
    Conditional random fields named entity classifier for multi-labeled input.
    """
    _metrics = {
        'accuracy': metrics.flat_accuracy_score,
        'precision': metrics.flat_precision_score,
        'recall': metrics.flat_recall_score,
        'f1': metrics.flat_f1_score
    }

    def fit(self, data, target):
        """
        Fits the models for each fold having fold models.

        Parameters
        ----------
        :param list[list[dict[str, T]]] data: The data, each element of the
            dictionary represents a word's features, a list of words represents
            a sentence.
        :param list[list[str]] target: the labels of each word for each
            sentence.
        """
        if not (isinstance(data, list) and isinstance(data[0], list)
                and isinstance(data[0][0], dict)):
            raise TypeError("The data is not of the correct type " +
                            "(list[list[dict[str, T]]]). If you want to use " +
                            "a dataset with a single element please consider " +
                            "wrapping the dataset as a list.")

        if self._folds > 1:
            kfold = KFold(n_splits=self._folds)

            for training_indices, _ in kfold.split(data):
                model = CRF(**self._model_config)\
                    .fit([data[i] for i in training_indices],
                         [target[i] for i in training_indices])
                self._models.append(model)
        else:
            model = CRF(**self._model_config).fit(data, target)
            self._models.append(model)

        return self

    def predict(self, data):
        """
        Returns the predictions of the model based on the voting of each of the
        trained models.

        Parameters
        ----------
        :param list[list[dict[str, T]]] data: The data, each element of the
            dictionary represents a word's features, a list of words represents
            a sentence.
        """
        if not (isinstance(data, list) and isinstance(data[0], list)
                and isinstance(data[0][0], dict)):
            raise TypeError("The data is not of the correct type " +
                            "(list[list[dict[str, T]]]). If you want to use " +
                            "a dataset with a single element please consider " +
                            "wrapping the dataset as a list.")

        predictions = [model.predict(data) for model in self._models]

        # Calculate the final predictions based on the voting of the folds
        predictions = zip(*predictions)
        predictions = (zip(*p) for p in predictions)
        predictions = [[max(set(p), key=p.count) for p in prediction]
                       for prediction in predictions]

        return predictions

    def evaluate(self, data, true, metric='accuracy', average=None):
        """
        Evaluates the performance of the model.

        Parameters
        ----------
        :param list[list[dict[str, T]]] data: The data, each element of the
            dictionary represents a word's features, a list of words represents
            a sentence.
        :param list[list[str]] true: the true labels of each word for each
             sentence.
        :param str metric: One of the possible metrics to use (accuracy,
            precision, recall, f1).
        :param str average: Type of average.
        """

        predictions = self.predict(data)

        if metric == 'accuracy':
            return self._metrics[metric](true, predictions)
        else:
            return self._metrics[metric](true, predictions, average=average)
