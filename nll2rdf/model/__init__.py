# -*- coding: utf-8 -*-
# Author: Cristian Cardellino

from __future__ import absolute_import, unicode_literals

from .crf import CRFNEClassifier

__all__ = ['CRFNEClassifier']
